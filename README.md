# AbstractFactory
This project is an example project to show a basic example of the Abstract Factory design pattern.


# A useful link
https://www.youtube.com/watch?v=v-GiuMmsXj4
Abstract Factory Pattern - Design Patterns (ep 5)
This video explains the Abstract Factory pattern using the Head First Design patterns book.


