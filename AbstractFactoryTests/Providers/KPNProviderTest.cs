﻿using NUnit.Framework;
using AbstractFactory.Providers;

namespace AbstractFactoryTests.Providers
{
    public class KPNProviderTest
    {
        [Test]
        public void Name_returns_ProviderName()
        {
            KPNProvider kpnProvider = new KPNProvider();

            Assert.AreEqual(kpnProvider.Name, "KPN");
        }

        [Test]
        public void AmountOfCustomers_returns_Integer()
        {
            KPNProvider kpnProvider = new KPNProvider();
            int i = 0;

            Assert.AreEqual(kpnProvider.AmountOfCustomers.GetType(), i.GetType());
        }

        [Test]
        public void AmountOfCustomers_updates()
        {
            //Arrange & sanity check
            KPNProvider kpnProvider = new KPNProvider();
            Assert.AreEqual(kpnProvider.AmountOfCustomers, 0);

            //Act
            kpnProvider.AmountOfCustomers += 100;

            //Assert
            Assert.AreEqual(kpnProvider.AmountOfCustomers, 100);
        }

        [Test]
        public void AddCustomer_updates_AmountOfCustomers()
        {
            //Arrange & sanity check
            KPNProvider kpnProvider = new KPNProvider();
            Assert.AreEqual(kpnProvider.AmountOfCustomers, 0);

            //Act
            kpnProvider.AddCustomer("Monthly");

            //Assert
            Assert.AreEqual(kpnProvider.AmountOfCustomers, 1);
        }
    }
}
