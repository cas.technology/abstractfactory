﻿using NUnit.Framework;
using AbstractFactory.Providers;

namespace AbstractFactoryTests.Providers
{
    public class ZiggoProviderTest
    {
        [Test]
        public void Name_returns_ProviderName()
        {
            ZiggoProvider ziggoProvider = new ZiggoProvider();

            Assert.AreEqual(ziggoProvider.Name, "Ziggo");
        }

        [Test]
        public void AmountOfCustomers_returns_Integer()
        {
            ZiggoProvider ziggoProvider = new ZiggoProvider();
            int i = 0;

            Assert.AreEqual(ziggoProvider.AmountOfCustomers.GetType(), i.GetType());
        }

        [Test]
        public void AmountOfCustomers_updates()
        {
            //Arrange & sanity check
            ZiggoProvider ziggoProvider = new ZiggoProvider();
            Assert.AreEqual(ziggoProvider.AmountOfCustomers, 0);

            //Act
            ziggoProvider.AmountOfCustomers += 100;

            //Assert
            Assert.AreEqual(ziggoProvider.AmountOfCustomers, 100);
        }

        [Test]
        public void AddCustomer_updates_AmountOfCustomers()
        {
            //Arrange & sanity check
            ZiggoProvider ziggoProvider = new ZiggoProvider();
            Assert.AreEqual(ziggoProvider.AmountOfCustomers, 0);

            //Act
            ziggoProvider.AddCustomer("Monthly");

            //Assert
            Assert.AreEqual(ziggoProvider.AmountOfCustomers, 1);
        }
    }
}
