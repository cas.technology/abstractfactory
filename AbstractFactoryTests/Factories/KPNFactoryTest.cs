using NUnit.Framework;
using AbstractFactory.Factories;
using AbstractFactory.Connections;

namespace AbstractFactoryTests.Factories
{
    public class KPNFactoryTest
    {
        [Test]
        public void CreateInternetConnection_returns_IInternetConnection()
        {
            //Arrange
            KPNFactory kpnFactory = new KPNFactory();

            //Act
            var actual = kpnFactory.CreateInternetConnection();
            var expected = new KPNInternetConnection();

            //Assert
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }
        [Test]
        public void CreatePhoneConnection_returns_IPhoneConnection()
        {
            //Arrange
            KPNFactory kpnFactory = new KPNFactory();

            //Act
            var actual = kpnFactory.CreatePhoneConnection();
            var expected = new KPNPhoneConnection();

            //Assert
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }
        [Test]
        public void CreateTVConnection_returns_ITVConnection()
        {
            //Arrange
            KPNFactory kpnFactory = new KPNFactory();

            //Act
            var actual = kpnFactory.CreateTVConnection();
            var expected = new KPNTVConnection();

            //Assert
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }
    }
}