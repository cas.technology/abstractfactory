﻿using NUnit.Framework;
using AbstractFactory.Factories;
using AbstractFactory.Connections;

namespace AbstractFactoryTests.Factories
{
    class ZiggoFactoryTest
    {
        [Test]
        public void CreateInternetConnection_returns_IInternetConnection()
        {
            //Arrange
            ZiggoFactory ziggoFactory = new ZiggoFactory();

            //Act
            var actual = ziggoFactory.CreateInternetConnection();
            var expected = new ZiggoInternetConnection();

            //Assert
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }
        [Test]
        public void CreatePhoneConnection_returns_IPhoneConnection()
        {
            //Arrange
            ZiggoFactory ziggoFactory = new ZiggoFactory();

            //Act
            var actual = ziggoFactory.CreatePhoneConnection();
            var expected = new ZiggoPhoneConnection();

            //Assert
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }
        [Test]
        public void CreateTVConnection_returns_ITVConnection()
        {
            //Arrange
            ZiggoFactory ziggoFactory = new ZiggoFactory();

            //Act
            var actual = ziggoFactory.CreateTVConnection();
            var expected = new ZiggoTVConnection();

            //Assert
            Assert.AreEqual(actual.GetType(), expected.GetType());
        }
    }
}
