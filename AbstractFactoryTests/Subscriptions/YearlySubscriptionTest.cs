﻿using NUnit.Framework;
using AbstractFactory.Subscriptions;
using AbstractFactory.Factories;
using AbstractFactory.Connections;

namespace AbstractFactoryTests.Subscriptions
{
    class YearlySubscriptionTest
    {
        [Test]
        public void AddCustomer_adds_Connections()
        {
            //Arrange
            ZiggoFactory ziggoFactory = new ZiggoFactory();
            MonthlySubscription monthlySubscription = new MonthlySubscription(ziggoFactory);

            //Act
            monthlySubscription.AddCustomer();

            //Assert
            Assert.AreEqual(monthlySubscription.PhoneConnection.GetType(), new ZiggoPhoneConnection().GetType());
            Assert.AreEqual(monthlySubscription.TVConnection.GetType(), new ZiggoTVConnection().GetType());
            Assert.AreEqual(monthlySubscription.InternetConnection.GetType(), new ZiggoInternetConnection().GetType());
        }
    }
}
