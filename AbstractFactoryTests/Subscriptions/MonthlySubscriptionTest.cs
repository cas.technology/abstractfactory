﻿using NUnit.Framework;
using AbstractFactory.Subscriptions;
using AbstractFactory.Factories;
using AbstractFactory.Connections;

namespace AbstractFactoryTests.Subscriptions
{
    class MonthlySubscriptionTest
    {
        [Test]
        public void AddCustomer_adds_Connections()
        {
            //Arrange
            KPNFactory kpnFactory = new KPNFactory();
            MonthlySubscription monthlySubscription = new MonthlySubscription(kpnFactory);

            //Act
            monthlySubscription.AddCustomer();

            //Assert
            Assert.AreEqual(monthlySubscription.PhoneConnection.GetType(), new KPNPhoneConnection().GetType());
            Assert.AreEqual(monthlySubscription.TVConnection.GetType(), new KPNTVConnection().GetType());
            Assert.AreEqual(monthlySubscription.InternetConnection.GetType(), new KPNInternetConnection().GetType());
        }
    }
}
