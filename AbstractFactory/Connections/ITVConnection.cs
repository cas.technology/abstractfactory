﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Connections
{
    public interface ITVConnection
    {
        public string ToString();
    }
}
