﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Connections
{
    public class ZiggoPhoneConnection : IPhoneConnection
    {
        public override string ToString()
        {
            return "Ziggo Phone";
        }
    }
}
