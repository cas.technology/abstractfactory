﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Connections
{
    public class KPNInternetConnection: IInternetConnection
    {
        public override string ToString()
        {
            return "KPN Internet";
        }
    }
}
