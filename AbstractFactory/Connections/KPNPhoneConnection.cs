﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Connections
{
    public class KPNPhoneConnection : IPhoneConnection
    {
        public override string ToString()
        {
            return "KPN Phone";
        }
    }
}
