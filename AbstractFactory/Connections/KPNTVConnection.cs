﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Connections
{
    public class KPNTVConnection : ITVConnection
    {
        public override string ToString()
        {
            return "KPN TV";
        }
    }
}
