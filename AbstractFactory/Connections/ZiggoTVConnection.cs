﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Connections
{
    public class ZiggoTVConnection : ITVConnection
    {
        public override string ToString()
        {
            return "Ziggo TV";
        }
    }
}
