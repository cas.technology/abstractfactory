﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Connections
{
    public class ZiggoInternetConnection : IInternetConnection
    {
        public override string ToString()
        {
            return "Ziggo Internet";
        }
    }
}
