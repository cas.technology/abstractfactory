﻿using AbstractFactory.Factories;
using AbstractFactory.Providers;
using System;
using System.Runtime.CompilerServices;
using System.Threading.Channels;

namespace AbstractFactory
{
    class Program
    {

        static void MainLoop(IProvider kpnProvider, IProvider ziggoProvider)
        {
        Console.Clear();
            Console.WriteLine("What would you like to do?");
            Console.WriteLine("1) Add a customer");
            Console.WriteLine("2) Show the current provider status");
            Console.WriteLine("3) Exit the application");
            var line = Console.ReadLine();
            if (line.Equals("1"))
            {
                AddCustomerAction(kpnProvider, ziggoProvider);
            } 
            else if (line.Equals("2"))
            {
                Console.WriteLine(kpnProvider.ToString());
                Console.WriteLine(ziggoProvider.ToString());
                Console.WriteLine("1) Continue");
                Console.WriteLine("2) Exit the application");
                var lineContinue = Console.ReadLine();
                if (lineContinue.Equals("1"))
                {
                    MainLoop(kpnProvider, ziggoProvider);
                }
                else if (lineContinue.Equals("2"))
                {
                    System.Environment.Exit(0);
                }
            } 
            else if (line.Equals("3"))
            {
                System.Environment.Exit(0);
            }
        }
        static void AddCustomerAction(IProvider kpnProvider, IProvider ziggoProvider)
        {
            Console.WriteLine("Add a customer to which provider?");
            Console.WriteLine("1) KPN");
            Console.WriteLine("2) Ziggo");
            Console.WriteLine("3) Cancel");
            var provider = Console.ReadLine();
            if (provider.Equals("3"))
            {
                MainLoop(kpnProvider, ziggoProvider);
            }
            Console.WriteLine("What type of contract does the customer need?");
            Console.WriteLine("1) Monthly");
            Console.WriteLine("2) Yearly");
            Console.WriteLine("3) Cancel");
            var type = Console.ReadLine();
            if (provider.Equals("1") && type.Equals("1"))
            {
                kpnProvider.AddCustomer("Monthly");
            } 
            else if (provider.Equals("1") && type.Equals("2"))
            {
                kpnProvider.AddCustomer("Yearly");
            } 
            else if (provider.Equals("2") && type.Equals("1"))
            {
                ziggoProvider.AddCustomer("Monthly");
            }
            else if (provider.Equals("2") && type.Equals("2"))
            {
                ziggoProvider.AddCustomer("Yearly");
            } 
            else if (type.Equals("3"))
            {
                MainLoop(kpnProvider, ziggoProvider);
            }
        }
        static void Main(string[] args)
        {

            var arr = new[]
           {

                    @" ,------. ,------.  ,-----.,--.   ,--.,--.,------.  ,------. ,------.  ",
                    @" |  .--. '|  .--. ''  .-.  '\  `.'  / |  ||  .-.  \ |  .---' |  .--.'  ",
                    @" | '--' | | '--'.' | |   | | \     /  |  ||  |  \  :|  `--,  |  '--'.' ",
                    @" |  | --' |  |\  \ '  '-'  '  \   /   |  ||  '--'  /|  `---. |  |\  \  ",
                    @" `--'     `--' '--' `-----'    `-'    `--'`-------' `------' `--' '--' ",
            };
            Console.WriteLine("\n\n");
            foreach (string linee in arr)
            {
                Console.WriteLine(linee);
            }
            Console.ReadKey();

            IProvider kpnProvider = new KPNProvider();
            IProvider ziggoProvider = new ZiggoProvider();

            while (true)
            {
                MainLoop(kpnProvider, ziggoProvider);
            }

        }
    }
}
