﻿using AbstractFactory.Connections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace AbstractFactory.Factories
{
    public class KPNFactory : AbstractConnectionFactory
    {
        public override IInternetConnection CreateInternetConnection()
        {
            return new KPNInternetConnection();
        }
        public override IPhoneConnection CreatePhoneConnection()
        {
            return new KPNPhoneConnection();
        }
        public override ITVConnection CreateTVConnection()
        {
            return new KPNTVConnection();
        }
    }
}
