﻿using AbstractFactory.Connections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace AbstractFactory.Factories
{
    public class ZiggoFactory : AbstractConnectionFactory
    {
        public override IInternetConnection CreateInternetConnection()
        {
            return new ZiggoInternetConnection();
        }
        public override IPhoneConnection CreatePhoneConnection()
        {
            return new ZiggoPhoneConnection();
        }
        public override ITVConnection CreateTVConnection()
        {
            return new ZiggoTVConnection();
        }
    }
}
