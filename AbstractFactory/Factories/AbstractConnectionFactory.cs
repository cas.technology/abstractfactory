﻿using AbstractFactory.Connections;
using System;


namespace AbstractFactory.Factories
{
    public abstract class AbstractConnectionFactory
    {
        public abstract IInternetConnection CreateInternetConnection();
        public abstract IPhoneConnection CreatePhoneConnection();
        public abstract ITVConnection CreateTVConnection();
    }
}
