﻿using AbstractFactory.Connections;
using AbstractFactory.Factories;
using AbstractFactory.Subscriptions;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace AbstractFactory.Providers
{
    public class ZiggoProvider : IProvider
    {
        public string Name
        {
            get
            {
                return "Ziggo";
            }
        }
        public int AmountOfCustomers { get; set; }
        public AbstractSubscription AddCustomer(string subscriptionTerm)
        {
            AbstractSubscription subscription;

            AbstractConnectionFactory connectionFactory = new KPNFactory();

            if (subscriptionTerm.Equals("Monthly"))
            {
                subscription = new MonthlySubscription(connectionFactory)
                {
                    Name = "Ziggo Monthly Subscription"
                };
            }
            else if (subscriptionTerm.Equals("Yearly"))
            {
                subscription = new YearlySubscription(connectionFactory)
                {
                    Name = "Ziggo Yearly Subscription"
                };
            }
            else
            {
                throw new NotImplementedException();
            }

            AmountOfCustomers += 1;

            return subscription;
        }
        public override string ToString()
        {
            return $"Provider {Name} has {AmountOfCustomers} customers.";
        }
    }
}
