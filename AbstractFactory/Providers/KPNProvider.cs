﻿using AbstractFactory.Connections;
using AbstractFactory.Factories;
using AbstractFactory.Subscriptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Providers
{
    public class KPNProvider : IProvider
    {
        public string Name
        {
            get
            {
                return "KPN";
            }
        }
        public int AmountOfCustomers { get; set; }
        public AbstractSubscription AddCustomer(string subscriptionTerm)
        {
            AbstractSubscription subscription;

            AbstractConnectionFactory connectionFactory = new KPNFactory();

            if (subscriptionTerm.Equals("Monthly"))
            {
                subscription = new MonthlySubscription(connectionFactory)
                {
                    Name = "KPN Monthly Subscription"
                };
            }
            else if (subscriptionTerm.Equals("Yearly"))
            {
                subscription = new YearlySubscription(connectionFactory)
                {
                    Name = "KPN Yearly Subscription"
                };
            }
            else
            {
                throw new NotImplementedException();
            }

            AmountOfCustomers += 1;

            return subscription;
        }
        public override string ToString()
        {
            return $"Provider {Name} has {AmountOfCustomers} customers.";
        }
    }
}
