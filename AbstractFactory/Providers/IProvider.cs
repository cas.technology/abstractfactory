﻿using AbstractFactory.Connections;
using AbstractFactory.Subscriptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Providers
{
    public interface IProvider
    {
        public string Name { get; }
        public int AmountOfCustomers { get; set; }
        public AbstractSubscription AddCustomer(string connectionType);
        public string ToString()
        {
            return $"Provider {Name} has {AmountOfCustomers} customers.";
        }
    }
}
