﻿using AbstractFactory.Connections;
using AbstractFactory.Factories;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Subscriptions
{
    public abstract class AbstractSubscription
    {
        public string Name { get; set; }
        public IPhoneConnection PhoneConnection { get; set; }
        public ITVConnection TVConnection { get; set; }
        public IInternetConnection InternetConnection { get; set; }
        public abstract void AddCustomer();
    }
}
