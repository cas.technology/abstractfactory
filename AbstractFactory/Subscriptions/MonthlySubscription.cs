﻿using AbstractFactory.Connections;
using AbstractFactory.Factories;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Subscriptions
{
    public class MonthlySubscription : AbstractSubscription
    {
        private AbstractConnectionFactory connectionFactory;
        public MonthlySubscription(AbstractConnectionFactory connectionFactory)
        {
            this.connectionFactory = connectionFactory;
        }
        public override void AddCustomer()
        {
            Console.WriteLine($"Creating connections for a Monthly subscription for {Name}");
            PhoneConnection = connectionFactory.CreatePhoneConnection();
            TVConnection = connectionFactory.CreateTVConnection();
            InternetConnection = connectionFactory.CreateInternetConnection();
        }
    }
}
