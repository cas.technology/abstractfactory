﻿using AbstractFactory.Factories;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Subscriptions
{
    public class YearlySubscription : AbstractSubscription
    {
        private AbstractConnectionFactory connectionFactory;
        public YearlySubscription(AbstractConnectionFactory connectionFactory)
        {
            this.connectionFactory = connectionFactory;
        }
        public override void AddCustomer()
        {
            Console.WriteLine($"Creating connections for a Yearly subscription for {Name}");
            PhoneConnection = connectionFactory.CreatePhoneConnection();
            TVConnection = connectionFactory.CreateTVConnection();
            InternetConnection = connectionFactory.CreateInternetConnection();
        }
    }
}
